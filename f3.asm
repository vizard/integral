;float f3 (float x)
;y = 5/x

section .rodata
  const1 dd 5.0

section .text
global f3
f3:
  push ebp      ;prolog
  mov ebp, esp

  finit
  fld dword[const1]
  fld dword[ebp+8]
  fdivp

  mov esp, ebp  ;epilog
  pop ebp
  ret
