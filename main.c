/* Практикум на ЭВМ: Задание №6
 * Примечания:
 * # Данные функции f1-f3, а также тестирующие функции f4-f6 описаны в
 * одноименных файлах .asm и объявлены в заголовочном файле functions.h
 * # Функции f1-f6 принимают и возвращают значение типа float
 * # Сборка программы осуществляется при помощи команды make, сборка
 * версии для отладки - make debug, очистка промежуточных файлов при сборке
 * финальной версии- make clean, при сборке отладочной версии - make clean-debug
 * # Исходный код содержит ряд инструкций, предназначенных для отладки программы,
 * cоответствующие области кода компилируются только при определении
 * идентификатора препроцессора DEBUG_MODE (определен при вызове make debug)
 * # Расшифровка кодов ошибок:
 * 1 - Некорректное количество аргументов тестирования
 * 2 - Некорректный ключ
 * 3 - Некорректный номер функции
 * */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "functions.h" // подключение функций f1, f2, f3
                       // (f4, f5, f6 - для тестрования функций root и integral)
#define HELP_KEY "-help"        // ключ вызова справки
#define ABSCISSA_KEY "-abs"     // ключ вывода абсцисс точек пересечения кривых
#define ITERATIONS_KEY "-itr"   // ключ вывода итераций поиска точек пересечения
#define TEST_INTEG_KEY "-testi" // ключ тестирования функции integral
#define TEST_ROOT_KEY "-testr"  // ключ тестирования функции root
#define EPSILON1 0.00001        // точность вычисления абсцисс точек пересечения
#define EPSILON2 0.0001         // точность вычисления интегралов
#define BEGIN_F12 5             // начало отрезка поиска точки пересечения f1 и f2
#define END_F12 7               // конец отрезка поиска точки пересечения f1 и f2
#define BEGIN_F23 3.8           // начало отрезка поиска точки пересечения f2 и f3
#define END_F23 5               // конец отрезка поиска точки пересечения f2 и f3
#define BEGIN_F13 0             // начало отрезка поиска точки пересечения f1 и f3
#define END_F13 5               // конец отрезка поиска точки пересечения f1 и f3

struct root_data { // структура, возвращаемая функцией root
  float result;    // найденный корень
  int iterations;  // число итераций
};

// поиск абсциссы точки пересечения функций f и g с точнотью eps1
// при помощи решения уравнения f-g=0 методом деления отрезка пополам
struct root_data root (float (*f)(float), float (*g)(float), float a, float b, float eps1);
// вычисление определенного интеграла от функции f с нижним/верхним пределами
// интегрирования a и b соответственно и точностью eps2 с помощью квадратурной
// формулы Симпсона (приближение параболами)
float integral (float (*f)(float), float a, float b, float eps2);
// вывод справки
void printHelp ();

/* Логические переменные (флаги) */
int printKeys;       // вывод справки
int printAbscissa;   // вывод абсцисс точек пересечения
int printIterations; // вывод числа итераций поиска точек пересечения
int testActive;      // режим тестирования (>0 - ф-я root <0 - ф-я integral
                     // ==0 - не активно)

/* Аргументы тестирования функций root и integral */
int testFunction1, testFunction2; // номера функций
    // (root использует testFunction1, integral - testFunction1&2)
float testBegin, testEnd; // начало & конец отрезка, содержащего корень
float testInaccuracy;     // точность вычислений

int 
main (int argc, char **argv)
{
  /* Анализ аргументов командной строки и выставление флагов
     (считывание аргументов тестирования) */
  for (int i = 1; i < argc; i++)
  {
    if (!strcmp((const char *)argv[i], HELP_KEY))
      printKeys = 1;
    else if (!strcmp((const char *)argv[i], ABSCISSA_KEY))
      printAbscissa = 1;
    else if (!strcmp((const char *)argv[i], ITERATIONS_KEY))
      printIterations = 1;
    else if (!strcmp((const char *)argv[i], TEST_ROOT_KEY))
    {
      testActive = 1;
      if (argc-i-1 != 5) // проверка наличия параметров тестирвования
      {
        printf("Некорректное количество аргументов тестирования\n");
        return 1;
      }
      // перевод строки в целое число
      testFunction1 = atoi((const char*)argv[i+1]);
      testFunction2 = atoi((const char *)argv[i+2]);
      // перевод строки в double
      testBegin = (float)strtod((const char *)argv[i+3], NULL);
      testEnd = (float)strtod((const char *)argv[i+4], NULL);
      testInaccuracy = (float)strtod((const char*)argv[i+5],NULL);
      i+=6; // смещение счетчика на количество считанных значений
    }
    else if (!strcmp((const char *)argv[i], TEST_INTEG_KEY))
    {
      testActive = -1;
      if (argc-i-1 != 4) // проверка наличия параметров тестирвования
      {
        printf("Некорректное количество аргументов тестирования\n");
        return 1;
      }
      // перевод строки в целое число
      testFunction1 = atoi((const char*)argv[i+1]);
      // перевод строки в double
      testBegin = (float)strtod((const char *)argv[i+2], NULL);
      testEnd = (float)strtod((const char *)argv[i+3], NULL);
      testInaccuracy = (float)strtod((const char*)argv[i+4],NULL);
      i+=5;  // смещение счетчика на количество считанных значений
    }
    else
    {
      printf("Некорректный ключ\n");
      return 2;
    }
  }

#ifdef DEBUG_MODE 
  /* Вывод считанных флагов и параметров */
  printf("Flags:\n printKeys= %d printAbscissa= %d printIterations= %d\n testActive= %d\n", printKeys, printAbscissa, printIterations, testActive);
  if (testActive)
    printf("Test arguments:\n testFunction1= %d testFunction2= %d\n"
            " testBegin= %f testEnd= %f testInaccuracy= %f\n", testFunction1, testFunction2, testBegin, testEnd, testInaccuracy);
  /* Тестирование функций f1-f6 */
  float a,b,c,d,e,f; 
  printf("Write arguments for f1,f2,f3,f4,f5,f6:\n");
  scanf("%f%f%f%f%f%f", &a, &b, &c, &d, &e, &f);
  printf("f1(%f): %f\nf2(%f): %f\nf3(%f): %f\nf4(%f): %f\nf5(%f): %f\nf6(%f): %f\n", a, f1(a), b, f2(b), c, f3(c), d, f4(d), e, f5(e), f, f6(f));
#endif

  if (printKeys)          // вывод справки
  {
    printHelp();
    return 0;
  }

  if (testActive > 0)     // тестирование root
  {
    struct root_data;          // возвращаемое значение root
    float(*first_arg)(float);  // указатель на первый функциональный параметр root
    float(*second_arg)(float); // на второй
    switch (testFunction1)     // перевод числовой нумерации функции в указатель
    {
      case 1:
      first_arg = f1;
      break;
      case 2:
      first_arg = f2;
      break;
      case 3:
      first_arg = f3;
      break;
      case 4:
      first_arg = f4;
      break;
      case 5:
      first_arg = f5;
      break;
      case 6:
      first_arg = f6;
      break;
      default: //здесь и далее - выход с ошибкой в случае несуществующей функции
      printf("Некорректный номер функции\n");
      return 3;
    }
    switch (testFunction2) // аналогично для второго функц. аргумента
    {
      case 1:
      second_arg = f1;
      break;
      case 2:
      second_arg = f2;
      break;
      case 3:
      second_arg = f3;
      break;
      case 4:
      second_arg = f4;
      break;
      case 5:
      second_arg = f5;
      break;
      case 6:
      second_arg = f6;
      break;
      default:
      printf("Некорректный номер функции\n");
      return 3;
    }
    printf("---Тестирование функции root---\n"
           "Результат: %f\n", root(first_arg, second_arg, testBegin, testEnd, testInaccuracy).result);
    return 0;
  }

  if (testActive < 0)     // тестирование integral
  {
    float(*first_arg)(float);
    switch (testFunction1) // перевод числовой нумерации функции в указатель
    {
      case 1:
      first_arg = f1;
      break;
      case 2:
      first_arg = f2;
      break;
      case 3:
      first_arg = f3;
      break;
      case 4:
      first_arg = f4;
      break;
      case 5:
      first_arg = f5;
      break;
      case 6:
      first_arg = f6;
      break;
      default:
      printf("Некорректный номер функции\n");
      return 3;
    }
     printf("---Тестирование функции integral---\n"
            "Результат: %f\n", integral(first_arg, testBegin, testEnd, testInaccuracy));
    return 0;
  }

  /* Результаты функции root для пар кривых fi, fg
     (f12 - пересечение функций f1&f2 и т.д.) */
  struct root_data f12 = root(f1,f2,BEGIN_F12,END_F12,EPSILON1);
  struct root_data f23 = root(f2,f3,BEGIN_F23,END_F23,EPSILON1);
  struct root_data f13 = root(f1,f3,BEGIN_F13,END_F13,EPSILON1);

  if (printIterations)    // вывод числа итераций поиска точек пересечения
  {
    printf("---Итерации---\n"
           "f1&f2: %d\n"
           "f2&f3: %d\n"
      	   "f1&f3: %d\n", f12.iterations, f23.iterations, f13.iterations);
  }

  if (printAbscissa)      // вывод абсцисс точек пересечения
  {
    printf("---Абсциссы точек пересечения---\n"
           "f1&f2: %f\nf2&f3: %f\nf1&f3: %f\n", f12.result, f23.result, f13.result);
  }
  
  /* Расчет и вывод искомой площади плоской фигуры */
  float area = integral(f1,f13.result,f12.result,EPSILON2)-integral(f2,f23.result,f12.result,EPSILON2)-integral(f3,f13.result,f23.result,EPSILON2);
  printf("Искомая площадь: %f\n", area);
  return 0;
}

void 
printHelp ()
{
  printf("---Ключи командной строки---\n"
         "* " HELP_KEY " - вывести этот список\n"
	     "* " ABSCISSA_KEY " - вывести абсциссы точек пересечения кривых\n"
         "* " ITERATIONS_KEY " - вывести число итераций, потребовавшихся для\n"
	     "нахождения точек пересечения\n"
	     "* " TEST_ROOT_KEY " <f,g,a,b,eps1> - режим тестирования функции root\n"
	     "* " TEST_INTEG_KEY " <f,a,b,eps2> - режим тестирования функции integral\n");
}

struct root_data
root (float (*f)(float), float(*g)(float), float a, float b, float eps1)
{ 
  struct root_data return_data = {0.0, 0}; // структура возвращаемых значений
  float lp = a, rp = b;   // левая и правая границы рассматриваемой "вилки"
  while (rp - lp >= eps1) // если длина "вилки" меньше заданной точности - можно
                          // взять любое значение отрезка в качеcтве корня
  {
    return_data.iterations++; // увелечение счетчика итераций
    float c = (rp + lp) / 2;  // деление "вилки" пополам
    if ((f(c) - g(c)) * (f(rp) - g(rp)) < 0) // выбор половины, содержащей корень,
                                             // в качестве следующей "вилки"
    {
      lp = c;
      continue;
    }
    rp = c;
  }
  return_data.result = (rp + lp) / 2; // возвращаемый корень - середина последней "вилки"
  return return_data;
}

float
integral (float (*f)(float), float a, float b, float eps2)
{
  float init_sum = f(a) + f(b);
  float FSum = init_sum; // хранит сумму значений функций в точках 
                         // деления сегмента [a,b]
  int n = 10;            // число отрезков, на которые делится сегмент [a,b]
  float h = (b - a) / n;
  float old_sum = 0;     // сумма значений функции в точках предыдущего
                         // разбиения сегмента
  for (int i = 1; i < n; i++)
  { 
    float newpoint = f(a + i * h);        // значение функции в i-ой точке разбиения
    old_sum += newpoint;                  // сохранение значения
    FSum += (2 + 2 * (i % 2)) * newpoint; // согласно формуле Симпсона значение функции
                                          // в точках с нечетными номерами домножается на 4,
				                          // с четными - на 2
  }
  float In = FSum * h / 3;  // вычисление приближенного значения интеграла
                            // при разбиении сегмента на n отрезков
  /* Поскольку при разбиении сегмента на 2n отрезков
   * точки разбиения сегмента на n отрезков являются
   * подмножеством нового множества точек разбиения,
   * то можно использовать вычисленные ранее значения функции
   * (домножив old_sum на два, получим сумму значений функции
   * в точках с четными номерами, домноженных на 2; f(a)+f(b)
   * также было вычислено ранее)
   * */
  FSum = old_sum * 2 + init_sum;
  n *= 2;          // разбиваем сегмент на 2n отрезков
  h = (b - a) / n; // пересчет h с учетом изменившегося n
  for (int i = 1; i < n; i += 2) // проход по всем нечетным точкам (четные вычислены ранее)
  {
    float newpoint = f(a + i * h);
    old_sum += newpoint;
    FSum += 4 * newpoint;
  }
  float I2n = FSum*h/3; // вычисление приближенного значения интеграла 
                        // при разбиении сегмента на 2n отрезков
  /* Увеличивая n в 2 раза на каждой итерации,
   * повторяем описанные ранее действия, вычисляя I(n)
   * и сохраняя I(n/2); согласно правилу Рунге вычисления
   * нужно продолжать до тех пор, пока (1/15)|I(n)-I(2n)| не
   * окажется меньше eps2
   * */
  while (abs(In - I2n) / 15 >= eps2)
  {
    In = I2n;
    FSum = old_sum * 2 + init_sum;
    n *= 2;
    h = (b - a) / n;
    for (int i = 1; i < n; i += 2)
    {
      float newpoint = f(a + i * h);
      old_sum += newpoint;
      FSum += 4 * newpoint;
    }
    I2n = FSum * h / 3;
  } 
  return I2n; //для большей точности в качестве возвращаемого значения используем I(2n)
}

