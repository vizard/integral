# Release version

all: AreaCalculation
AreaCalculation: main.o f1.o f2.o f3.o f4.o f5.o f6.o
	gcc -m32 -o AreaCalculation main.o f1.o f2.o f3.o f4.o f5.o f6.o
main.o: main.c functions.h
	gcc -m32 -c -std=c99 -o main.o main.c
f1.o: f1.asm
	nasm -f elf32 -o f1.o f1.asm
f2.o: f2.asm
	nasm -f elf32 -o f2.o f2.asm
f3.o: f3.asm
	nasm -f elf32 -o f3.o f3.asm
f4.o: f4.asm
	nasm -f elf32 -o f4.o f4.asm
f5.o: f5.asm
	nasm -f elf32 -o f5.o f5.asm
f6.o: f6.asm
	nasm -f elf32 -o f6.o f6.asm
clean:
	rm -v f1.o f2.o f3.o f4.o f5.o f6.o main.o

# Debug version - adds debug information to object files and compiles 
# main.c with DEBUG_MODE identificator

debug: AreaCalculation-debug
AreaCalculation-debug: main-debug.o f1-debug.o f2-debug.o f3-debug.o f4-debug.o f5-debug.o f6-debug.o
	gcc -m32 -g -o AreaCalculation-debug main-debug.o f1-debug.o f2-debug.o f3-debug.o f4-debug.o f5-debug.o f6-debug.o
main-debug.o: main.c functions.h
	gcc -m32 -c -std=c99 -g -D=DEBUG_MODE -o main-debug.o main.c
f1-debug.o: f1.asm
	nasm -f elf32 -g -o f1-debug.o f1.asm
f2-debug.o: f2.asm
	nasm -f elf32 -g -o f2-debug.o f2.asm
f3-debug.o: f3.asm
	nasm -f elf32 -g -o f3-debug.o f3.asm
f4-debug.o: f4.asm
	nasm -f elf32 -g -o f4-debug.o f4.asm
f5-debug.o: f5.asm
	nasm -f elf32 -g -o f5-debug.o f5.asm
f6-debug.o: f6.asm
	nasm -f elf32 -g -o f6-debug.o f6.asm
clean-debug:
	rm -v f1-debug.o f2-debug.o f3-debug.o f4-debug.o f5-debug.o f6-debug.o main-debug.o


		
