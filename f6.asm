;TEST_FUNCTION - F6(float x)
;f6 = sqrt(x)

section .text
global f6
f6:
  push ebp      ;prolog
  mov ebp, esp

  finit
  fld dword[ebp+8]
  fsqrt

  mov esp, ebp  ;epilog
  pop ebp
  ret
