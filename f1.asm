;float f1 (float x)
;y = 3(0.5/(x1)+1)

section .rodata
  const1 dd 1.5
  const2 dd 3.0

section .text
global f1
f1:
  push ebp      ;prolog
  mov ebp, esp

  finit
  fld dword[ebp+8]
  fld1
  faddp
  fld dword[const1]
  fxch
  fdivp
  fld dword[const2]
  faddp

  mov esp, ebp  ;epilog
  pop ebp
  ret
