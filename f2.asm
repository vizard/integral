;float f2 (float x)
;y = 2.5x - 9.5

section .rodata
  const1 dd 2.5
  const2 dd 9.5

section .text
global f2
f2:
  push ebp      ;prolog
  mov ebp, esp

  finit
  fld dword[const1]
  fld dword[ebp+8]
  fmulp
  fld dword[const2]
  fsubp

  mov esp, ebp  ;epilog
  pop ebp
  ret
