;FOR TESTING - float f4 (float x)
;f4 = x^2

section .text
global f4
f4:
  push ebp      ;prolog
  mov ebp, esp

  finit
  fld dword[ebp+8]
  fld dword[ebp+8]
  fmulp

  mov esp, ebp  ;epilog
  pop ebp
  ret
