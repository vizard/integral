;FOR TESTING - float f5 (float x)
;f5 = -0.5x + 3

section .rodata
  const1 dd -0.5
  const2 dd 3

section .text
global f5
f5:
  push ebp      ;prolog
  mov ebp, esp

  finit
  fld dword[const1]
  fld dword[ebp+8]
  fmulp
  fld dword[const2]
  faddp

  mov esp, ebp  ;epilog
  pop ebp
  ret
